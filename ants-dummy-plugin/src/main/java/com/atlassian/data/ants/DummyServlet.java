package com.atlassian.data.ants;

import com.atlassian.data.ants.api.Ants;
import com.atlassian.data.ants.api.Entry;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import java.io.IOException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Named
public class DummyServlet extends HttpServlet
{
    private final Ants ants;

    @Inject
    public DummyServlet(@SuppressWarnings ("SpringJavaAutowiringInspection") @ComponentImport final Ants ants)
    {
        this.ants = ants;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException
    {
        Entry<String> value = ants.getString("myKey");
        final Entry<String> prevValue;
        if (value == null)
        {
            prevValue = null;
            value = ants.putString("myKey", null);
        }
        else
        {
            prevValue = ants.getString("myKey", value.version());
            value = ants.putString("myKey", "myValue " + (value.version() + 1), value.version());
        }

        resp.setContentType("text/plain");
        resp.getWriter().write("cur:  " + value.value() + " " + value.version() + " " + value.date() + "\n");
        if (prevValue != null)
        {
            resp.getWriter().write("prev: " + prevValue.value() + " " + prevValue.version() + " " + value.date() + "\n");
        }
    }
}
