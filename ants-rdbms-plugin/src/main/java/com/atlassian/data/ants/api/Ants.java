package com.atlassian.data.ants.api;

import javax.annotation.Nonnull;

public interface Ants
{
    Entry<String> getString(@Nonnull String key);

    Entry<String> getString(@Nonnull String key, int version);

    Entry<String> putString(@Nonnull String key, String value);

    Entry<String> putString(@Nonnull String key, String value, int currentVersion);
}
