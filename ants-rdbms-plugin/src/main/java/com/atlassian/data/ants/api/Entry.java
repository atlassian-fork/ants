package com.atlassian.data.ants.api;

import java.util.Date;

/**
 * Represents an immutable entry - a specific version of a value
 */
public interface Entry<T> {

    /**
     * User specified key
     */
    String key();

    /**
     * Current value
     */
    T value();

    /**
     * Version starts at 0 and is incremented
     */
    int version();

    /**
     * System assigned date
     */
    Date date();
}
