package com.atlassian.data.ants.rdbms.db;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import javax.sql.DataSource;

/**
 * {@link javax.sql.DataSource} that allows nothing but <code>getConnection</code> for the supplied connection.
 */
public class FlywayDataSource implements DataSource
{
    private final Connection connection;

    public FlywayDataSource(@Nonnull final Connection connection)
    {
        this.connection = connection;
    }

    @Override
    public Connection getConnection() throws SQLException
    {
        return connection;
    }

    @Override
    public Connection getConnection(final String username, final String password) throws SQLException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setLogWriter(final PrintWriter out) throws SQLException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setLoginTimeout(final int seconds) throws SQLException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getLoginTimeout() throws SQLException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> T unwrap(final Class<T> iface) throws SQLException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isWrapperFor(final Class<?> iface) throws SQLException
    {
        throw new UnsupportedOperationException();
    }
}
