package com.atlassian.data.ants.rdbms.db;

public class Names
{
    public static String TABLE_PREFIX = "ANTS";
    public static String TABLE_SUFFIX_KEY = "KEY";
    public static String TABLE_SUFFIX_VALUE = "VALUE";
    public static String TABLE_SUFFIX_VERSIONS = "VERSIONS";

    public static String COL_PREFIX = TABLE_PREFIX;

    public static String COL_KEY = "KEY";
    public static String COL_SHA1 = "SHA1";
    public static String COL_VERSION = "VERSION";
    public static String COL_DATE = "DATE";
    public static String COL_VALUE = "VALUE";
}
