package com.atlassian.data.ants.rdbms.provider;

import com.atlassian.data.ants.api.Entry;
import com.atlassian.data.ants.rdbms.service.EntryImpl;
import com.google.common.hash.Hashing;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.annotation.Nonnull;

import static com.atlassian.data.ants.rdbms.db.Names.*;
import static com.google.common.base.Preconditions.checkNotNull;

public class StringProvider
{
    private static final String KEY = COL_PREFIX + "_" + COL_KEY;
    private static final String VALUE = COL_PREFIX + "_" + COL_VALUE;
    private static final String VERSION = COL_PREFIX + "_" + COL_VERSION;
    private static final String SHA1 = COL_PREFIX + "_" + COL_SHA1;
    private static final String DATE = COL_PREFIX + "_" + COL_DATE;

    private final String SQL_INSERT_KEY;
    private final String SQL_INSERT_VALUE;
    private final String SQL_INSERT_VERSION;
    private final String SQL_SELECT_ALL_BY_KEY_VERSION;
    private final String SQL_SELECT_LATEST_VERSION_BY_KEY;

    public StringProvider(@Nonnull final String id)
    {
        checkNotNull(id);

        final String tableKey = TABLE_PREFIX + "_" + id + "_" + TABLE_SUFFIX_KEY;
        final String tableValue = TABLE_PREFIX + "_" + id + "_" + TABLE_SUFFIX_VALUE;
        final String tableVersions = TABLE_PREFIX + "_" + id + "_" + TABLE_SUFFIX_VERSIONS;

        this.SQL_INSERT_KEY = ""
                + "INSERT INTO " + tableKey + " "
                + "(" + KEY + ") "
                + "VALUES (?) ";

        this.SQL_INSERT_VALUE = ""
                + "INSERT INTO " + tableValue + " "
                + "(" + SHA1 + ", " + VALUE + ") "
                + "VALUES (?, ?) ";

        this.SQL_INSERT_VERSION = ""
                + "INSERT INTO " + tableVersions + " "
                + "(" + KEY + ", " + VERSION + ", " + SHA1 + ", " + DATE + ") "
                + "VALUES (?, ?, ?, ?) ";

        this.SQL_SELECT_ALL_BY_KEY_VERSION = ""
                + "SELECT ve." + KEY + ", ve." + VERSION + ", ve." + SHA1 + ", ve." + DATE + " , va." + VALUE + " "
                + "FROM " + tableVersions + " as ve "
                + "LEFT JOIN " + tableValue + " AS va "
                + "ON va." + SHA1 + " = ve." + SHA1 + " "
                + "WHERE ve." + KEY + " = ? "
                + "AND ve." + VERSION + " = ? ";

        this.SQL_SELECT_LATEST_VERSION_BY_KEY = ""
                + "SELECT MAX(" + VERSION + ") "
                + "FROM " + tableVersions + " "
                + "WHERE " + KEY + " = " + " ? ";
    }

    public Entry<String> get(@Nonnull final Connection connection, @Nonnull final String key)
    {
        // TODO @alex may be optimised to a single select
        final Integer latestVersion = latestVersionByKey(connection, key);
        if (latestVersion == null)
        {
            return null;
        }
        else
        {
            return get(connection, key, latestVersion);
        }
    }

    public Entry<String> get(@Nonnull final Connection connection, @Nonnull final String key, final int version)
    {
        try
        {
            final PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_BY_KEY_VERSION);
            statement.setString(1, key);
            statement.setInt(2, version);

            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                return new EntryImpl<>(
                        resultSet.getString(KEY),
                        resultSet.getString(VALUE),
                        resultSet.getInt(VERSION),
                        new Date(resultSet.getLong(DATE)));
            }
            else
            {
                return null;
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException("everything's fucked", e);
        }
    }

    public Entry<String> put(@Nonnull Connection connection, @Nonnull final String key, @Nonnull final String value)
    {
        final Integer latestVersion = latestVersionByKey(connection, key);
        if (latestVersion == null)
        {
            throw new RuntimeException("value already present");
        }

        try
        {
            final PreparedStatement insertKey = connection.prepareStatement(SQL_INSERT_KEY);
            insertKey.setString(1, key);
            insertKey.execute();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("everything's fucked", e);
        }

        return putNewValue(connection, key, value, 0);
    }

    public Entry<String> put(@Nonnull Connection connection, @Nonnull final String key, @Nonnull final String value, final int version)
    {
        final Integer latestVersion = latestVersionByKey(connection, key);
        if (latestVersion != version)
        {
            throw new RuntimeException("version mismatch");
        }

        return putNewValue(connection, key, value, version + 1);
    }

    private Entry<String> putNewValue(final Connection connection, final String key, final String value, final int version)
    {
        final byte[] sha1 = Hashing.sha1().hashString(value, Charset.forName("UTF-8")).asBytes();
        final Date date = new Date();

        try
        {
            // TODO @alex detect duplicate value
            final PreparedStatement insertValue = connection.prepareStatement(SQL_INSERT_VALUE);
            insertValue.setBytes(1, sha1);
            insertValue.setString(2, value);
            insertValue.execute();

            final PreparedStatement insertVersion = connection.prepareStatement(SQL_INSERT_VERSION);
            insertVersion.setString(1, key);
            insertVersion.setInt(2, version);
            insertVersion.setBytes(3, sha1);
            insertVersion.setLong(4, date.getTime());
            insertVersion.execute();

            return new EntryImpl<>(key, value, 0, date);
        }
        catch (SQLException e)
        {
            throw new RuntimeException("everything's fucked", e);
        }
    }

    private Integer latestVersionByKey(final Connection connection, final String key)
    {
        try
        {
            final PreparedStatement statement = connection.prepareStatement(SQL_SELECT_LATEST_VERSION_BY_KEY);
            statement.setString(1, key);
            final ResultSet resultSet = statement.executeQuery();

            return resultSet.next() ? resultSet.getInt(1) : null;
        }
        catch (SQLException e)
        {
            throw new RuntimeException("everything's fucked", e);
        }
    }
}
