package it.com.atlassian.data.ants.service;

import com.atlassian.data.ants.api.Entry;
import com.atlassian.data.ants.rdbms.db.Bootstrap;
import com.atlassian.data.ants.rdbms.provider.StringProvider;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class AntsServiceTest
{
    private static String ID = "ABCDEF";

    private static Connection connection;

    private StringProvider stringProvider;

    @BeforeClass
    public static void beforeClass() throws SQLException
    {
        connection = DriverManager.getConnection("jdbc:h2:tcp://127.0.0.1:9092//tmp/antsdb", "sa", "");
        Bootstrap.execute(connection, ID);
    }

    @Before
    public void before()
    {
        try
        {
            CallableStatement statement = connection.prepareCall("delete from ANTS_" + ID + "_VERSIONS");
            statement.execute();
        }
        catch (SQLException e)
        {
            // too bad
        }
        try
        {
            CallableStatement statement = connection.prepareCall("delete from ANTS_" + ID + "_KEY");
            statement.execute();
        }
        catch (SQLException e)
        {
            // too bad
        }
        try
        {
            CallableStatement statement = connection.prepareCall("delete from ANTS_" + ID + "_VALUE");
            statement.execute();
        }
        catch (SQLException e)
        {
            // too bad
        }
        stringProvider = new StringProvider(ID);
    }

    @Test
    public void putNew()
    {
        final Entry<String> put = stringProvider.put(connection, "someKey", "someValue");

        assertThat(put.key(), is("someKey"));
        assertThat(put.value(), is("someValue"));
        assertThat(put.version(), is(0));
        assertThat(put.date(), lessThanOrEqualTo(new Date()));
    }

    @Test
    public void getLatestNew()
    {
        final Entry<String> put = stringProvider.put(connection, "someKey", "someValue");

        final Entry<String> get = stringProvider.get(connection, "someKey");

        assertThat(get.key(), is(put.key()));
        assertThat(get.value(), is(put.value()));
        assertThat(get.version(), is(put.version()));
        assertThat(get.date(), is(put.date()));
    }

    @Test
    public void getVersionedNew()
    {
        final Entry<String> put = stringProvider.put(connection, "someKey", "someValue");

        final Entry<String> get = stringProvider.get(connection, "someKey", 0);

        assertThat(get.key(), is(put.key()));
        assertThat(get.value(), is(put.value()));
        assertThat(get.version(), is(put.version()));
        assertThat(get.date(), is(put.date()));
    }

    @Test
    public void getLatestNull()
    {
        assertThat(stringProvider.get(connection, "someKey"), is(nullValue()));
    }

    @Test
    public void getVersionedNull()
    {
        assertThat(stringProvider.get(connection, "someKey", 0), is(nullValue()));
    }
}
